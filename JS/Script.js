//API YOUTUBE

     // 2. This code loads the IFrame Player API code asynchronously.
     var tag = document.createElement('script');
     tag.src = "https://www.youtube.com/iframe_api";
     var firstScriptTag = document.getElementsByTagName('script')[0];
     firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

     // 3. This function creates an <iframe> (and YouTube player)
     //    after the API code downloads.
     var player;
     var i= 0;
     function onYouTubeIframeAPIReady() {
       player = new YT.Player('player', {
         height: '360',
         width: '640',
         videoId: playList[i] || "GjkBjQUzLUs",
         events: {
           'onReady': onPlayerReady,
           'onStateChange': onPlayerStateChange
         }
       });
     }

     // 4. The API will call this function when the video player is ready.
     function onPlayerReady(event) {
       event.target.playVideo();
     
     }

     // 5. The API calls this function when the player's state changes.
     //    The function indicates that when playing a video (state=1),
     //    the player should play for six seconds and then stop.
     var done = false;
     function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.ENDED) {
          player.loadVideoById(playList[++i],0,'large');
        }
     }
     function stopVideo() {
       player.stopVideo();
     }
   $(document).ready(function(){
      $('.list-item').click(function(){
        var url = $(this).data('url');
        player.loadVideoById(url,0,'large');
      });

   });
    


//FORMULAIRE DE CONNEXION


$(document).ready(function(){
    $('#connect').click(function(){
        $('#connect-form').css("display","block");
    });
    $('#close-con').click(function(){
      $('#connect-form').css("display","none");
    });
});


//FORMULAIRE INSCRIPTION

$('document').ready(function(){
  $('#subscribe').click(function(){
    $('#inscription').css("display","block");
  });
   $('#close-ins').click(function(){
    $('#inscription').css("display","none");
   }); 
});
   