<?php

class User{
    private $id;
    private $pseudo;
    private $password;
    
   //getters & setters 
    
    public function __construct($pseudo,$password){
        $this->id=uniqid();
        $this->setPseudo($pseudo);
        $this->setPassword($password);
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getPseudo(){
        return $this->pseudo;
    }
    
    public function getPassword(){
        return $this->password;
    }
    
    public function setPseudo($value){
        $this->pseudo = $value;
    }
    
    public function setPassword($valueP){
        $this->password = $valueP;
    }
}


class playlist{
    private $id;
    private $user_id;
    
    //getters & setters
    
    public function __construct($UsrId){
        $this->id=uniqid();
        $this->setUser_id($UsrId);
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUser_id(){
        return $this->user_id;
    }
    
    public function setUser_id(){
        User::getId();
    }
    
}


class Video{
    private $id;
    private $userId;
    private $url;
    
    //getters & setters 
    
    public function __construct($userID,$Url){
        $this->id=uniqid();
        $this->setUserId($userID);
        $this->setUrl($Url);
    }
    
    public function getId(){
        return $this->id;
    }
    
    public function getUserId(){
        return $this->userId;
    }
    
    public function getUrl(){
        return $this->url;
    }
    
    public function setUserId($value){
        $this->userId=$value;
    }
    
    public function setUrl($value){
        $this->url = $value;
    }
}
?>