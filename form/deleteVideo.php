<?php
include_once('../DB/dbAccess.php');
include_once('../class/class.php');
session_start();


$del=$_POST['delId'];
$userId = $_SESSION['userId'];


function deleteVideo($del,$userId){
    $bdd = Db::connexion();
    $req=$bdd->prepare('DELETE FROM `videos` WHERE id = :id AND user_id= :userid');
    $req->execute(array(
        'id' => $del,
        'userid'=>$userId
    ));
}

deleteVideo($del,$userId);

header('Location: ../index.php');

?>