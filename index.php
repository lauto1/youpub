<?php 
        include_once('DB/dbAccess.php');
        include_once('class/class.php');
        session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Youpub</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <script defer src="https://use.fontawesome.com/releases/v5.0.4/js/all.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="assets/indexStyle.css">
    <script src="JS/Script.js" type="text/javascript" ></script>
</head>
<body>
    
<header class="navbar navbar-expand-sm navbar-expand-md navbar-expand-lg navbar-light bg-dark col-12 justify-content-between">
            <h1 class="col-4">YouPub</h1>
                <form action="form/addVideo.php" method="POST" class="col-4">
                    <input type="text" name="newUrl" class="col-8">
                    <input type="submit" value="add" class="btn btn-success">
                </form>
            <div class="ml-5 row col-3 d-flex justify-content-around">
                <h5 id="connect">Connexion</h5>
                <h5 id="subscribe">Inscription</h5>
                <a href="form/disconnect.php"><i id="sign-out"class="fas fa-sign-out-alt fa-2x"></i></a>
            </div>
        </header>
    <div class="container-fluid col-md-10 col-lg-10">
        
        <section>
            <h1>Nom de la Playlist</h1>
            <article class="row col-sm-12 col-md-12 col-lg-12">
                
            <div class="row col-md-10">
                <button class="btn btn-primary">
                    <i class="fas fa-step-backward fa-3x"></i>
                </button>
                <div id="player"></div>
                <button class="btn btn-primary">
                    <i class="fas fa-step-forward fa-3x"></i>
                </button>
            </div>
            <div class="card col-md-2">
                <div>info video</div>
                <div class="row">
                    partager + supprimer
                </div>
            </div>
            </article>
        </section>
        <!--liste de videos -->
        <article style="border:solid 2px">
            <?php
            $userId = $_SESSION['userId'];
             $bdd=Db::connexion();
             $req=$bdd->query("SELECT `id`,`url` FROM `videos` WHERE `user_id`='".$userId."'");
             $result=$req->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Video",["id","url"]);
             //var_dump($result);
         
             foreach($result as $a){
                 $url = $a->getUrl();
                 $tab[]= $a->getUrl();
                 $id = $a->getId();

                 echo "
                 <div class='list-item' data-url='$url'>
                        <h4>titre video</h4>
                     <form action='form/deleteVideo.php' method='POST'>
                        <input type='hidden' name='delId' value='$id'>
                        <input type='hidden' name='url' value='$url'>
                        <input type='image'src='' value='submit'>
                     </form>
                     <div>zone de priorité +1/-1</div>
                 </div>";
                 
                
             }
            // print_r($tab);
            
            ?>
            <script>
                var playList= <?php echo json_encode($tab);?>;
                //console.log(playList);
            </script>
        </article>
        <footer></footer>
    </div>
    <section id="connect-form" class="col-md-12 pt-5">
            <div class="card col-md-5 mx-auto mt-5 form-group">
                <div class="d-flex row justify-content-end">
                    <div id="close-con"><i class="far fa-times-circle fa-2x"></i></div>
                </div>
                <h3 class="mx-auto card-header col-12 text-center">Sign in Please</h3>
                <form action="form/connect.php" method="POST" class="mx-auto col-8">
                    <input class="form-control col-12 mx-auto my-2" type="text" name="pseudo" placeholder="pseudo">
                    <input class="form-control col-12 mx-auto my-2" type="password" name="passW" placeholder="password">
                    <input class="btn btn-primary col-12 mx-auto my-3" type="submit">
                </form>
            </div>
        </section>
    
        <section class="col-md-12 pt-5" id="inscription">
            <div class="mx-auto col-md-5 text-center">
                <div class="d-flex row justify-content-end">
                 <div id="close-ins"><i class="far fa-times-circle fa-2x"></i></div>
                </div>
                <h3 class="mx-auto card-header col-12 text-center">Sign up now!</h3>
                <form action="form/registerScript.php" method="POST" class="mx-auto col-8">
                    <input type="text" name="pseudo" placeholder="Pseudo" class="form-control col-12 mx-auto my-2">
                    <input type="password" name="password" placeholder="Password" class="form-control col-12 mx-auto my-2">
                    <input type="password" name="pwdConfirm" placeholder="confirmation" class="form-control col-12 mx-auto my-2">
                    <input type="submit" class="btn btn-primary col-12 mx-auto my-3">
                </form>
            </div>
    </section>
</body>
</html>